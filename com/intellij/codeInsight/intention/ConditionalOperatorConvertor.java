package com.intellij.codeInsight.intention;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.SystemInfo;
import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import com.sun.javafx.tools.packager.Log;
import jetbrains.buildServer.messages.serviceMessages.TestStdOut;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by avdhesh on 27/01/16.
 */
public class ConditionalOperatorConvertor extends PsiElementBaseIntentionAction implements IntentionAction, com.intellij.openapi.components.ProjectComponent {
    int index = 0;
    StringBuilder builder = new StringBuilder();
    ArrayList<Integer> linenumbers = new ArrayList<>();
    ArrayList<Integer> indexes = new ArrayList<>();
    int datastoreindex = 0;
    String context ;
    int spaces=0;

    @Override
    public void projectOpened() {

    }

    @Override
    public void projectClosed() {

    }

    @Override
    public void initComponent() {

    }

    @Override
    public void disposeComponent() {

    }

    @NotNull
    @Override
    public String getComponentName() {
        return "Lint_Fix_I2";
    }

    @NotNull
    public String getText() {
        return "DataStore.put entries";
    }

    @NotNull
    public String getFamilyName() {
        return getText();
    }
    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, @NotNull PsiElement element) {
        if (element == null) return false;
        if (!element.isWritable()) return false;
        String source = editor.getDocument().getCharsSequence().toString();
        String[] elements = element.toString().split(":");
        String contentvalues = "DataStore.putContentValues";

            if(element instanceof PsiIdentifier){
                final PsiIdentifier identifiertoken = (PsiIdentifier) element;
                if (identifiertoken.toString().contains("DataStore") || identifiertoken.toString().contains("put")) {
                    if(source.contains(contentvalues)){
                        if(elements[1].equals("DataStore") || elements[1].equals("putContentValues")){
                            return false;
                        }
                    }
                    return true;
                }
                return false;
            }


        return false;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, @NotNull PsiElement element) throws IncorrectOperationException {
        //Access document, caret, and selection
        final Document document = editor.getDocument();
        String source = document.getCharsSequence().toString();
        linenumbers.clear();
        indexes.clear();
        int occurence =0;
        if(!builder.toString().contains("ContentValues values = new ContentValues();")) {
            builder.append("ContentValues values = new ContentValues();");
        }
        if(source.contains("DataStore.putLong")){
            indexes.add(source.indexOf("DataStore.putLong"));
        }
        if(source.contains("DataStore.putString")){
            indexes.add(source.indexOf("DataStore.putString"));
        }
        if(source.contains("DataStore.putBoolean")){
            indexes.add(source.indexOf("DataStore.putBoolean"));
        }
        if(source.contains("DataStore.putInt")){
            indexes.add(source.indexOf("DataStore.putInt"));
        }

        for(int i=0; i < source.length(); i++) {
            occurence++;
            String[] elements = element.toString().split(":");
            index = source.indexOf(elements[1], index + 1);

            datastoreindex = source.indexOf("DataStore", datastoreindex+1);
            if(source.contains("DataStore.putContentValues") && datastoreindex == source.indexOf("DataStore.putContentValues")){
                continue;
            }
            if (index != -1) {
                int linenumber = document.getLineNumber(index);

                if (!linenumbers.contains(linenumber)) {
                    linenumbers.add(linenumber);
                    String line = source.substring(document.getLineStartOffset(linenumber), document.getLineEndOffset(linenumber));
                    String parentheses = line.substring(line.indexOf("(")+1,line.indexOf(")"));
                    String [] split = parentheses.split(",");
                    String context = split[0].trim();
                    String key = split[1].trim();
                    String value = split[2].trim();
                    System.out.println(context+" "+key+" "+value);
                    this.context = context;
                    spaces = datastoreindex - document.getLineStartOffset(linenumber);
                    if(!builder.toString().contains("values.put("+key+", "+value+");")){
                        builder.append("\n");
                        for(int j=0;j < (datastoreindex - document.getLineStartOffset(linenumber)); j++){
                            builder.append(" ");
                        }
                        builder.append("values.put("+key+", "+value+");");
                    }

                }
            }
        }
        if(!builder.toString().contains("DataStore.putContentValues("+context+", values);")) {
           builder.append("\n");
           for(int j=0;j < spaces; j++){
                builder.append(" ");
            }
            builder.append("DataStore.putContentValues(" + context + ", values);");
        }
        System.out.println(builder.toString());
        final SelectionModel selectionModel = editor.getSelectionModel();
        final int start = selectionModel.getSelectionStart();
        final int end = selectionModel.getSelectionEnd();
        //Making the replacement
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //document.replaceString(document.getLineStartOffset(linenumbers.get(0)), document.getLineEndOffset(linenumbers.get(linenumbers.size()-1)),
                //        builder.toString());
                Collections.sort(linenumbers, new Comparator<Integer>() {
                    @Override
                    public int compare(Integer int2, Integer int1){
                        return  int2.compareTo(int1);
                    }
                });
                for(int i = 0; i < linenumbers.size()-1 ; i++){
                    if(linenumbers.get(i+1) > linenumbers.get(i)+1){
                        System.out.println("yes");
                        document.replaceString(document.getLineStartOffset(document.getLineNumber(start)), document.getLineEndOffset(document.getLineNumber(end)),
                                builder.toString());
                        builder = new StringBuilder();
                        linenumbers.clear();
                        indexes.clear();
                        selectionModel.removeSelection();
                        return;
                    }
                }
                int minindex = Collections.min(indexes);
                System.out.println(minindex);
                document.replaceString(minindex, document.getLineEndOffset(linenumbers.get(linenumbers.size()-1)),
                        builder.toString());
                builder = new StringBuilder();
                linenumbers.clear();
                indexes.clear();
            }
        };
        WriteCommandAction.runWriteCommandAction(project, runnable);

    }
}
